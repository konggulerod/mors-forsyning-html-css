﻿$(".calculateForm input").on("change", function () {
    var gas = 0;
    if ($('#gas').val()) {
        var gas = parseInt($("#gas").val());
    }
    var gasMWH = gas / 91;
    var oile = 0;
    if ($('#oile').val()) {
        var oile = parseInt($("#oile").val());
    }
    var oileMWH = oile / 125;
    var house = 0;
    if ($('#house').val()) {
        var house = parseInt($("#house").val());
    }

    var gasAndOileMWH = gasMWH + oileMWH;

    var varibleTarif = parseInt($("#varibleTarif").val());
    var fixedAmout = $("#fixedAmout").val();
    var meter = parseInt($("#meter").val());

    var varibleTarifAmount = gasAndOileMWH.toFixed() * varibleTarif;
    var fixedAmoutTotal = house * fixedAmout;
    var checkType = 0;
    if ($('input[name="checktype"]').is(':checked')) {
        checkType = parseInt($('input[name="checktype"]:checked').val());
    }
    var total = meter + varibleTarifAmount + fixedAmoutTotal + checkType;

    console.log("Variabel tarif " + varibleTarifAmount);
    console.log("Fast bidrag " + fixedAmoutTotal.toFixed(2));
    console.log("Type " + checkType)
    console.log("Total udgift " + total)

    if (isNaN(total)) {
        $(".heatResult").text(0);
    } else {
        $(".heatResult").text(total.toFixed(2));
    }
    
});