﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using umbraco;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace MorsoeForsyning.Controllers
{
    public class RelocationFormViewModel
    {
        [Required]
        public string CurrentPlaceAddress { get; set; }
        [Required]
        public string CurrentPlaceZipCode { get; set; }
        [Required]
        public string CurrentPlaceCity { get; set; }
        [Required]
        public string VacatingName { get; set; }
        [Required]
        public string VacatingNewAddress { get; set; }
        [Required]
        public string VacatingNewZipCode { get; set; }
        [Required]
        public string VacatingNewCity { get; set; }
        [Required]
        public string VacatingIdentification { get; set; }
        [Required]
        public string VacatingPhone { get; set; }
        [Required]
        public string VacatingEmail { get; set; }
        [Required]
        public string ReadingMeterNumber { get; set; }
        [Required]
        public string Reading { get; set; }
        [Required]
        public string AcqusitionDate { get; set; }
        [Required]
        public string IncomingName { get; set; }
        [Required]
        public string IncomingPreviousAddress { get; set; }
        [Required]
        public string IncomingPreviousZipCode { get; set; }
        [Required]
        public string IncomingPreviousCity { get; set; }
        [Required]
        public string IncomingIdentification { get; set; }
        [Required]
        public string IncomingPhone { get; set; }
        [Required]
        public string IncomingEmail { get; set; }
    }
    public class RelocationController : SurfaceController
    {
        [RequireHttps]
        public ActionResult Index(RelocationFormViewModel model)
        {
            if (ModelState.IsValid) {
                string body = string.Format("<h1>Anmeldt flytning</h1><p>Der er på websitet blevet anmeldt en flytning.</p><h2>Forbrugssted</h2>"
                                            + "Adresse: {0}<br />"
                                            + "Postnummer: {1}<br />"
                                            + "By: {2}"
                                            + "<h2>Fraflytter</h2>"
                                            + "Navn: {3}<br />"
                                            + "<strong>Ny adresse</strong><br />"
                                            + "Adresse: {4}<br />"
                                            + "Postnummer: {5}<br />"
                                            + "By: {6}<br />"
                                            + "<strong>Oplysninger</strong><br />"
                                            + "CPR/CVR: {7}<br />"
                                            + "Telefon: {8}<br />"
                                            + "E-mail: {9}"
                                            + "<h2>Aflæsning</h2>"
                                            + "Målernummer: {10}<br />"
                                            + "Aflæsning: {11}<br />"
                                            + "Overtagelsesdato: {12}"
                                            + "<h2>Tilflytter</h2>"
                                            + "Navn: {13}<br />"
                                            + "<strong>Tidligere adresse</strong><br />"
                                            + "Adresse: {14}<br />"
                                            + "Postnummer: {15}<br />"
                                            + "By: {16}<br />"
                                            + "<strong>Oplysninger</strong><br />"
                                            + "CPR/CVR: {17}<br />"
                                            + "Telefon: {18}<br />"
                                            + "E-mail: {19}<br />", model.CurrentPlaceAddress, model.CurrentPlaceZipCode, model.CurrentPlaceCity, model.VacatingName, model.VacatingNewAddress, model.VacatingNewZipCode, model.VacatingNewCity,
                                                model.VacatingIdentification, model.VacatingPhone, model.VacatingEmail, model.ReadingMeterNumber, model.Reading, model.AcqusitionDate, model.IncomingName, model.IncomingPreviousAddress,
                                                model.IncomingPreviousCity, model.IncomingPreviousZipCode, model.IncomingIdentification, model.IncomingPhone, model.IncomingEmail
                                            );
                var companyEmail = "flej@morsoeforsyning.dk";
                library.SendMail("noreply@morsoeforsyning.dk", companyEmail, "Flytning anmeldt: " + model.VacatingName, body, true);
                return CurrentUmbracoPage();
            }
            return CurrentUmbracoPage();
        }
    }
    public class DistrictheatingFormViewModel
    {
        [Required]
        public string PreeName { get; set; }
        [Required]
        public string PreeAddress { get; set; }
        [Required]
        public string PreePhone { get; set; }
        [Required]
        public string PreeEmail { get; set; }
        [Required]
        public string PreeContact { get; set; }
        [Required]
        public string PreeNewAddress { get; set; }

    }
    public class DistrictheatingFormController : SurfaceController
    {
        [RequireHttps]
        public ActionResult Index(DistrictheatingFormViewModel model)
        {
            if (ModelState.IsValid)
            {
                string body = string.Format("<h1>forhåndstilmelding</h1><br /><h2>Installationsadresse</h2>"
                                            + "Installationsadresse : {5}<br />"
                                            + "Ejers navn: {0}<br />"
                                            + "Ejers adresse: {1}<br />"
                                            + "Telefon: {2}<br />"
                                            + "Email: {3}<br />"
                                            + "Kontaktperson: {4}<br />", model.PreeName, model.PreeAddress, model.PreePhone, model.PreeEmail, model.PreeContact, model.PreeNewAddress
                                            );
                var companyEmail = "fjernvarme@morsoeforsyning.dk";
                library.SendMail("noreply@morsoeforsyning.dk", companyEmail, "Forhåndstilmelding " + model.PreeName, body, true);
                return CurrentUmbracoPage();
            }
            return CurrentUmbracoPage();
        }
    }
}
