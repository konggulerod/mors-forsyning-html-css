﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Mvc;

namespace MorsoeForsyning.Controllers
{
    public class SearchController : SurfaceController
    {
        [HttpGet]
        public ActionResult Index(string q)
        {
            var searchString = q.ToLower().Trim();
            if (searchString.Any()) {
                var hiddenDocTypes = new[] { "errorPages", "language" };
                var results = Umbraco.TypedSearch(searchString);
                results = results
                    .Where(i => !hiddenDocTypes.Contains(i.DocumentTypeAlias)
                        && i.HasProperty("searchHide") && !i.GetPropertyValue<bool>("searchHide")
                    );
                return PartialView("~/Views/Partials/Search.cshtml", results);
            }
            return Content("");
        }
    }
}
