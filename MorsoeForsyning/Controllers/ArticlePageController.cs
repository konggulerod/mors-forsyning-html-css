﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Umbraco.Web;
using Umbraco.Web.Models;
using Umbraco.Web.Mvc;

namespace MorsoeForsyning.Controllers
{
    public class ArticlePageController : RenderMvcController
    {
        [HttpGet]
        public override ActionResult Index(RenderModel model)
        {
            if (model.Content.HasValue("password")) {
                return View("~/Views/PassAuthenticate.cshtml", model);
            }
            return base.Index(model);
        }
        [HttpPost]
        public ActionResult Index(RenderModel model, string password)
        {
            if (model.Content.HasValue("password") && !string.IsNullOrEmpty(password)) {
                var pagePassword = model.Content.GetPropertyValue<string>("password");
                if (password == pagePassword) {
                    return base.Index(model);
                }
            }
            return View("~/Views/PassAuthenticate.cshtml", model);
        }
    }
}
